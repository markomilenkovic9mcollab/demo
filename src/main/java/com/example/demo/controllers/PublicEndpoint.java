package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@RestController
public class PublicEndpoint {

    @Autowired
    private HttpServletRequest request;

    @GetMapping(value = "radi")
    public String radi() {
        return "Radi";
    }

    @GetMapping(value = "neradi")
    public String neradi() {
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
            System.out.println("ne radi");
        }
        System.out.println("usaoooooooooooooo");
        return "neradi";
    }

}
